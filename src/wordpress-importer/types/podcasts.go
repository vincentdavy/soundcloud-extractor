package types

import (
	"context"
	"fmt"
	"github.com/robbiet480/go-wordpress"
	"log"
	"net/http"
	"soundcloud-extractor/src/commons"
	"time"
)

const (
	podcastURL    = "podcast"
	episodeType   = "audio"
	publishStatus = "publish"
)

//FindPodcastByID find a podcast by ID
func FindPodcastByID(podcastID int, getWPClient func() *wordpress.Client) bool {
	ctx := context.Background()
	resp, err := getWPClient().Get(ctx, fmt.Sprintf(podcastURL+"/%d", podcastID), nil, nil)
	defer closeBody(resp)

	if err == nil && resp != nil && resp.StatusCode == http.StatusOK {
		return true
	}

	return false
}

//CheckPodcastExists check if podcast exists in wordpress
func CheckPodcastExists(search string, getWPClient func() *wordpress.Client) int {
	returnedPodcasts := make([]commons.PodcastType, 0)
	ctx := context.Background()
	resp, err := getWPClient().List(ctx, podcastURL, struct {
		Slug string `url:"slug"`
	}{
		Slug: Slugify(search),
	}, &returnedPodcasts)
	defer closeBody(resp)

	if resp == nil || resp.StatusCode != http.StatusOK {
		panic(err)
	}

	if len(returnedPodcasts) == 0 {
		log.Printf("Erronous podcast length found %d for search %s\n", len(returnedPodcasts), search)
		return 0
	}

	log.Printf("Found podcast id %d for search %s", returnedPodcasts[0].ID, returnedPodcasts[0].Slug)
	return returnedPodcasts[0].ID
}

//CreatePodcast create podcast in wordpress
func CreatePodcast(title, audioFile string, serieID, featuredMediaID int, duration uint64, getWPClient func() *wordpress.Client) int {

	ctx := context.Background()

	var createdPodcast commons.PodcastType
	resp, err := getWPClient().Create(ctx, podcastURL, commons.PodcastType{
		Title:         title,
		Serie:         []int{serieID},
		FeaturedMedia: featuredMediaID,
		Status:        publishStatus,
		Meta: struct {
			EpisodeType string `json:"episode_type"`
			AudioFile   string `json:"audio_file"`
			Duration    string `json:"duration"`
		}{
			episodeType, audioFile, formatDuration(duration),
		},
	}, &createdPodcast)
	defer closeBody(resp)

	if resp == nil || resp.StatusCode != http.StatusCreated {
		panic(err)
	}

	log.Printf("Created podcast id %d for serie ID %d", createdPodcast.ID, serieID)
	return createdPodcast.ID
}

func formatDuration(durAsInt uint64) string {
	dur := time.Duration(durAsInt) * time.Millisecond
	dur = dur.Round(time.Second)

	if dur >= time.Hour {
		h := dur / time.Hour
		dur -= h * time.Hour
		m := dur / time.Minute
		dur -= m * time.Minute
		s := dur / time.Second

		return fmt.Sprintf("%d:%02d:%02d", h, m, s)
	}
	m := dur / time.Minute
	dur -= m * time.Minute
	s := dur / time.Second

	return fmt.Sprintf("%d:%02d", m, s)
}
