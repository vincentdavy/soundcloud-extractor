package types

import (
	"fmt"
	"testing"
	"time"
)

func TestFormatDuration(t *testing.T) {
	durToTest := 2*time.Hour + 6*time.Minute + 4*time.Second
	durToTest /= 1000 * 1000
	fmted := formatDuration(uint64(durToTest))
	fmt.Println(fmted)
	if fmted != "2:06:04" {
		t.Error("Wrong date formatted")
	}
}

func TestFormatDuration_Long(t *testing.T) {
	durToTest := 2*time.Hour + 16*time.Minute + 14*time.Second
	durToTest /= 1000 * 1000
	fmted := formatDuration(uint64(durToTest))
	fmt.Println(fmted)
	if fmted != "2:16:14" {
		t.Error("Wrong date formatted")
	}
}

func TestFormatDurationMinute(t *testing.T) {
	durToTest := 6*time.Minute + 4*time.Second
	durToTest /= 1000 * 1000
	fmted := formatDuration(uint64(durToTest))
	fmt.Println(fmted)
	if fmted != "6:04" {
		t.Error("Wrong date formatted")
	}
}
