package types

import (
	"context"
	"fmt"
	"github.com/robbiet480/go-wordpress"
	"log"
	"net/http"
	"soundcloud-extractor/src/commons"
)

const seriesURL = "series"

//CheckSerieExists check if serie exists in wordpress
func CheckSerieExists(search string, getWPClient func() *wordpress.Client) int {
	returnedSeries := make([]commons.SerieType, 0)
	ctx := context.Background()
	resp, err := getWPClient().List(ctx, seriesURL, struct {
		Slug string `url:"slug"`
	}{
		Slug: Slugify(search),
	}, &returnedSeries)
	defer closeBody(resp)

	if resp == nil || resp.StatusCode != http.StatusOK {
		panic(err)
	}

	if len(returnedSeries) == 0 {
		log.Printf("Erronous serie length found %d for search %s\n", len(returnedSeries), search)
		return 0
	}

	log.Printf("Found serie id %d for search %s", returnedSeries[0].ID, returnedSeries[0].Slug)
	return returnedSeries[0].ID
}

//CreateSerie create serie in wordpress
func CreateSerie(name string, getWPClient func() *wordpress.Client) int {
	ctx := context.Background()

	var createdSerie commons.SerieType
	resp, err := getWPClient().Create(ctx, seriesURL, commons.SerieType{
		Name:        name,
		Description: name,
	}, &createdSerie)
	defer closeBody(resp)

	if resp == nil || resp.StatusCode != http.StatusCreated {
		panic(err)
	}

	log.Printf("Created serie id %d for playlist %s", createdSerie.ID, name)
	return createdSerie.ID
}

//FindSerieByID find a serie by ID
func FindSerieByID(serieID int, getWPClient func() *wordpress.Client) bool {
	ctx := context.Background()
	resp, err := getWPClient().Get(ctx, fmt.Sprintf(seriesURL+"/%d", serieID), nil, nil)
	defer closeBody(resp)

	if err == nil && resp != nil && resp.StatusCode == http.StatusOK {
		return true
	}

	return false
}
