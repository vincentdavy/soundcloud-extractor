package types

import "soundcloud-extractor/src/commons"

//MediaCheck required infos to trigger a media exist check
type MediaCheck struct {
	SearchTerms,
	MediaType,
	FilePath string
	MediaIDInterface commons.MediaID
}
