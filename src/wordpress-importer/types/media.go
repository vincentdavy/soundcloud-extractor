package types

import (
	"context"
	"fmt"
	"github.com/robbiet480/go-wordpress"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
)

const (
	imageName = "%s.jpeg"
	soundName = "%s.mp3"
	// AudioMediaType wordpress API audio media type
	AudioMediaType = "audio"
	// ImageMediaType wordpress API image media type
	ImageMediaType = "image"
)

var (
	mimeTypes = map[string]string{
		AudioMediaType: "audio/mpeg",
		ImageMediaType: "image/jpeg",
	}
)

//CreateMediaIfNotExisting check if a media exists and create if not
func CreateMediaIfNotExisting(mediaCheckChan <-chan MediaCheck, mediaCheckWG *sync.WaitGroup, id int, getWPClient func() *wordpress.Client) {
	defer mediaCheckWG.Done()
	log.Printf("Starting media check worker id %d\n", id)
	for check := range mediaCheckChan {
		if check.FilePath != "" {
			mediaID := checkMediaExists(check.SearchTerms, check.MediaType, getWPClient)
			if mediaID == 0 {
				mediaID = createMedia(check, getWPClient)
			}

			if check.MediaType == ImageMediaType {
				check.MediaIDInterface.SetImageMediaID(mediaID)
			} else {
				check.MediaIDInterface.SetSoundMediaID(mediaID)
			}
		}
	}
	log.Printf("Stopping media check id %d\n", id)
}

func createMedia(media MediaCheck, getWPClient func() *wordpress.Client) int {
	// prepare file to upload
	file, err := os.Open(media.FilePath)
	defer file.Close()
	if err != nil {
		log.Fatalf("Failed to open media file %s to upload: %v", media.FilePath, err.Error())
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Failed to read media file %s to upload: %v", media.FilePath, err.Error())
	}

	var fileName string
	if media.MediaType == ImageMediaType {
		fileName = fmt.Sprintf(imageName, strings.Replace(media.SearchTerms, "/", "-", -1))
	} else {
		fileName = fmt.Sprintf(soundName, strings.Replace(media.SearchTerms, "/", "-", -1))
	}

	for i := 0; i < 10; i++ {
		ctx := context.Background()
		mediaCreated, resp, err := getWPClient().Media.Create(ctx, &wordpress.MediaUploadOptions{
			Filename:    fileName,
			ContentType: mimeTypes[media.MediaType],
			Data:        fileContents,
		})
		defer closeBody(resp)
		if resp != nil || resp.StatusCode == http.StatusCreated {
			log.Printf("Created media %s (%s) - id %d", mediaCreated.Title, fileName, mediaCreated.ID)
			return mediaCreated.ID
		}

		log.Printf("Error creating media %s : %s - retrying...", fileName, err)
	}

	log.Fatalf("Failed to create media file %s", fileName)
	return 0
}

func checkMediaExists(search, mediaType string, getWPClient func() *wordpress.Client) int {
	ctx := context.Background()
	medias, resp, err := getWPClient().Media.List(ctx, &wordpress.MediaListOptions{
		MediaType: mediaType,
		ListOptions: wordpress.ListOptions{
			Search: Slugify(search),
		},
	})
	defer closeBody(resp)
	if resp == nil || resp.StatusCode != http.StatusOK {
		panic(err)
	}

	if len(medias) == 0 || len(medias) > 1 {
		log.Printf("Erronous media length found %d for search %s\n", len(medias), search)
		return 0
	}

	log.Printf("Found id %d for search %s", medias[0].ID, medias[0].Slug)
	return medias[0].ID
}

//FindByID find a media by id and return true if found
func FindByID(mediaID int, getWPClient func() *wordpress.Client) bool {
	ctx := context.Background()
	_, resp, err := getWPClient().Media.Get(ctx, mediaID, nil)
	defer closeBody(resp)
	if err == nil && resp != nil && resp.StatusCode == http.StatusOK {
		return true
	}

	return false
}

//GetMediaSourceURL return the source URL of a media
func GetMediaSourceURL(mediaID int, getWPClient func() *wordpress.Client) string {
	ctx := context.Background()
	media, resp, err := getWPClient().Media.Get(ctx, mediaID, nil)
	defer closeBody(resp)
	if resp == nil || resp.StatusCode != http.StatusOK {
		panic(err)
	}

	return media.SourceURL
}

//Slugify format the string to have it matching slug
func Slugify(mediaName string) string {
	return strings.Replace(strings.Replace(mediaName, "/", "-", -1), " ", "-", -1)
}

func closeBody(resp *wordpress.Response) {
	if resp != nil && resp.Body != nil {
		resp.Body.Close()
	}
}
