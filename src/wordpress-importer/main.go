package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"soundcloud-extractor/src/commons"
	"soundcloud-extractor/src/wordpress-importer/types"
	"sync"
)

var (
	version string // set at build time
	commit  string // set at build time
	date    string // set at build time

	wpUser       string // flag
	wpPassword   string // flag
	fileBaseDir  string // flag
	workersCount int    //flag
)

const (
	wordpressURL        = "https://www.station-millenium.com/wp-json/"
	defaultWorkersCount = 3
)

func init() {
	flag.StringVar(&wpUser, "wpUser", "", "Wordpress user")
	flag.StringVar(&wpPassword, "wpPassword", "", "Wordpress password")
	flag.StringVar(&fileBaseDir, "fileBaseDir", commons.FileBaseDirDefault, "File base dir")
	flag.IntVar(&workersCount, "workers", defaultWorkersCount, "Workers count")
}

func main() {
	log.Printf("Version : %s - Commit : %s - Date : %s", version, commit, date)
	flag.Parse()
	if wpUser == "" || wpPassword == "" {
		flag.PrintDefaults()
		log.Fatalln("Missing parameters")
	}

	commons.InitDir(fileBaseDir)

	playlists := unmarshallData()
	playlistsDest := copyPlaylists(playlists)

	mediaCheck(playlists, playlistsDest, workersCount)

	saveJSONFile(playlistsDest)
	playlists = copyPlaylists(playlistsDest)[:]

	seriesCheck(playlists)

	saveJSONFile(playlists)
	log.Println("Series check done")

	podcastCheck(playlists)

	saveJSONFile(playlists)
	log.Println("Media check done")
}

func podcastCheck(playlists []commons.PlaylistType) {
	for i := range playlists {
		for j := range playlists[i].Tracks {
			track := &playlists[i].Tracks[j]
			if track.PodcastID == 0 || !types.FindPodcastByID(track.PodcastID, getWPClient) {
				if podcastID := types.CheckPodcastExists(track.Title, getWPClient); podcastID == 0 {
					log.Printf("Podcast creation for track ID %d needed", track.ID)
					mediaSourceURL := types.GetMediaSourceURL(track.SoundMediaID, getWPClient)
					log.Printf("Podcast ID %d have sound URL %s (ID : %d)", track.ID, mediaSourceURL, track.SoundMediaID)
					track.PodcastID = types.CreatePodcast(track.Title, mediaSourceURL, playlists[i].SerieID, track.ArtworkMediaID, track.Duration, getWPClient)
				} else {
					track.PodcastID = podcastID
					log.Printf("Track ID %d have exitsing podcast ID %d", track.ID, podcastID)
				}
			} else {
				log.Printf("Track ID %d have already podcast ID %d", track.ID, track.PodcastID)
			}
		}
	}
}

func seriesCheck(playlists []commons.PlaylistType) {
	for i := range playlists {
		if playlists[i].SerieID == 0 || !types.FindSerieByID(playlists[i].SerieID, getWPClient) {
			if serieID := types.CheckSerieExists(playlists[i].Title, getWPClient); serieID == 0 {
				log.Printf("Serie creation for playlist ID %d needed", playlists[i].SerieID)
				playlists[i].SerieID = types.CreateSerie(playlists[i].Title, getWPClient)
			} else {
				playlists[i].SerieID = serieID
				log.Printf("Playlist ID %d have exitsing serie ID %d", playlists[i].ID, serieID)
			}
		} else {
			log.Printf("Playlist ID %d have already serie ID %d", playlists[i].ID, playlists[i].SerieID)
		}
	}
}

func saveJSONFile(playlists []commons.PlaylistType) {
	if jsonFile, err := os.Create(commons.JSONFilePath); err == nil {
		json.NewEncoder(jsonFile).Encode(playlists)
		log.Printf("File writed at %s\n", commons.JSONFilePath)
	} else {
		panic(err)
	}
}

func mediaCheck(playlists []commons.PlaylistType, playlistsDest []commons.PlaylistType, workersCount int) {
	var mediaCheckWG sync.WaitGroup
	var mediaCheckChan = make(chan types.MediaCheck)
	mediaCheckWG.Add(workersCount)
	for i := 0; i < workersCount; i++ {
		go types.CreateMediaIfNotExisting(mediaCheckChan, &mediaCheckWG, i, getWPClient)
	}
	for indexPlaylist, playlist := range playlists {
		if playlist.ArtworkMediaID == 0 || !types.FindByID(playlist.ArtworkMediaID, getWPClient) {
			mediaCheckChan <- types.MediaCheck{
				playlist.Title, types.ImageMediaType, playlist.ArtworkFile, &playlistsDest[indexPlaylist],
			}
		} else {
			log.Printf("Playlist ID %d have already artwork ID %d", playlist.ID, playlist.ArtworkMediaID)
		}
		for indexTrack, track := range playlist.Tracks {
			if track.ArtworkMediaID == 0 || !types.FindByID(track.ArtworkMediaID, getWPClient) {
				mediaCheckChan <- types.MediaCheck{
					track.Title, types.ImageMediaType, track.ArtworkFile, &playlistsDest[indexPlaylist].Tracks[indexTrack],
				}
			} else {
				log.Printf("Track ID %d have already track ID %d", track.ID, track.ArtworkMediaID)
			}
			if track.SoundMediaID == 0 || !types.FindByID(track.SoundMediaID, getWPClient) {
				mediaCheckChan <- types.MediaCheck{
					track.Title, types.AudioMediaType, track.SoundFile, &playlistsDest[indexPlaylist].Tracks[indexTrack],
				}
			} else {
				log.Printf("Track ID %d have already sound ID %d", track.ID, track.SoundMediaID)
			}
		}
	}
	close(mediaCheckChan)
	mediaCheckWG.Wait()
	log.Println("Media check done")
}
