package main

import (
	"encoding/json"
	"github.com/robbiet480/go-wordpress"
	"os"
	"soundcloud-extractor/src/commons"
)

func unmarshallData() []commons.PlaylistType {
	var playlists []commons.PlaylistType
	if playlistFile, err := os.Open(commons.JSONFilePath); err == nil {
		json.NewDecoder(playlistFile).Decode(&playlists)
	} else {
		panic(err)
	}
	return playlists
}

func getWPClient() *wordpress.Client {
	tp := wordpress.BasicAuthTransport{
		Username: wpUser,
		Password: wpPassword,
	}

	client, err := wordpress.NewClient(wordpressURL, tp.Client())
	if err != nil {
		panic(err)
	}
	return client
}

func copyPlaylists(source []commons.PlaylistType) []commons.PlaylistType {
	dest := make([]commons.PlaylistType, len(source))
	copy(dest, source)
	for i, playlist := range source {
		dest[i].Tracks = make([]commons.TrackType, len(playlist.Tracks))
		copy(dest[i].Tracks, playlist.Tracks)
	}
	return dest
}
