package commons

var (
	//PlaylistDir the dir of a playlist
	PlaylistDir string

	//TrackDir the dir of a track
	TrackDir string

	//PlaylistImageFile the template for a playlist image path
	PlaylistImageFile string

	//TrackImageFile the template for a track image path
	TrackImageFile string

	//JSONFilePath the path for playlist JSON file
	JSONFilePath string

	//TrackSoundFile the template for a track sound path
	TrackSoundFile string
)

//InitDir init all the dirs and templates based on base path
func InitDir(fileBaseDir string) {
	PlaylistDir = fileBaseDir + "/%d"
	TrackDir = PlaylistDir + "/%d"
	PlaylistImageFile = PlaylistDir + "/%d.jpg"
	TrackImageFile = TrackDir + "/%d.jpg"
	TrackSoundFile = TrackDir + "/%d.mp3"
	JSONFilePath = fileBaseDir + "/playlists.json"
}
