package commons

const (
	//FileBaseDirDefault base dir of all files
	FileBaseDirDefault = "/tmp/soundcloud-extractor"
)

//MediaID set the ID of wordpress medias for types
type MediaID interface {
	SetImageMediaID(id int)
	SetSoundMediaID(id int)
}

//SetImageMediaID set the image media ID for playlist
func (playlist *PlaylistType) SetImageMediaID(id int) {
	playlist.ArtworkMediaID = id
}

//SetSoundMediaID do nothing - playlist has no sound media
func (playlist *PlaylistType) SetSoundMediaID(id int) {}

//SetImageMediaID set the image media ID for tracks
func (track *TrackType) SetImageMediaID(id int) {
	track.ArtworkMediaID = id
}

//SetSoundMediaID set the sound media ID for tracks
func (track *TrackType) SetSoundMediaID(id int) {
	track.SoundMediaID = id
}

// PlaylistType playlist type
type PlaylistType struct {
	ID             uint64
	Title          string
	ArtworkURL     string
	Tracks         []TrackType
	ArtworkFile    string
	ArtworkMediaID int
	SerieID        int
}

//TrackType the track type
type TrackType struct {
	ID             uint64
	Title          string
	ArtworkURL     string
	SoundURL       string
	ArtworkFile    string
	SoundFile      string
	Duration       uint64
	ArtworkMediaID int
	SoundMediaID   int
	PodcastID      int
}

//SerieType the serie type in wordpress
type SerieType struct {
	ID          int      `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Slug        string   `json:"slug"`
	Parent      int      `json:"parent"`
	Meta        []string `json:"meta"`
}

//PodcastType the podcast type in wordpress
type PodcastType struct {
	ID            int    `json:"id,omitempty"`
	Title         string `json:"title"`
	Serie         []int  `json:"series"`
	FeaturedMedia int    `json:"featured_media"`
	Slug          string `json:"slug"`
	Status        string `json:"status"`
	Meta          struct {
		EpisodeType string `json:"episode_type"`
		AudioFile   string `json:"audio_file"`
		Duration    string `json:"duration"`
	} `json:"meta"`
}
