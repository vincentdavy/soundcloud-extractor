SHELL := /bin/bash

VERSION=`git describe --exact-match --tags HEAD 2> /dev/null`
COMMIT=`git rev-parse HEAD 2> /dev/null`
DATE_BUILD=`date +%Y-%m-%d\_%H:%M`

GOBIN = $(GOPATH)/bin
GOLINT = $(GOBIN)/golint
DEP = $(GOBIN)/dep

.PHONY: all lint vendor test cover build clean help fmt

all: build

$(DEP):
	go get -u github.com/golang/dep/cmd/dep

vendor: $(DEP) ## Install dependencies
	dep ensure

$(GOLINT):
	go get -u github.com/golang/lint/golint

fmt: ## Make gofmt persistant
	gofmt -s -w -l src/

lint: $(GOLINT) ## Start lint
	diff -u <(echo -n) <(gofmt -s -d src/); [ $$? -eq 0 ]
	go tool vet src/**/*.go
	diff -u <(echo -n) <(golint src/...); [ $$? -eq 0 ]

test: vendor ## Run test
	go test -race ./src/...

cover: vendor ## Display test coverage percent
	./cover.sh -terminal

build: vendor ## Build binary
	go build -race -ldflags "-X main.version=${VERSION} -X main.commit=${COMMIT} -X main.date=${DATE_BUILD}" -o "build/soundcloud-extractor" src/soundcloud-extractor/*.go
	go build -race -ldflags "-X main.version=${VERSION} -X main.commit=${COMMIT} -X main.date=${DATE_BUILD}" -o "build/wordpress-importer" src/wordpress-importer/*.go

clean: ## Remove vendors and build
	rm -rf vendor
	rm -rf build

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
